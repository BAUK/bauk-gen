# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'bauk/gen/version'

Gem::Specification.new do |spec|
  spec.name          = 'bauk-gen'
  spec.version       = Bauk::Gen::VERSION
  spec.authors       = ['Kuba Jasko']
  spec.email         = ['kubajasko@hotmail.co.uk']

  spec.summary       = 'Generator framework wrapping erb.'
  spec.description   = 'It is config driven and extensible, while also allows different inputs and outputs.' \
                       'You can create gems that this will pull in, enabling adding in custom templates and options'
  spec.homepage      = 'https://gitlab.com/BAUK/bauk-gen'
  spec.license       = 'MIT'

  spec.metadata['allowed_push_host'] = "https://rubygems.org"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/BAUK/bauk-gen'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/BAUK/bauk-gen/commits/master'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      f.match(%r{^(test|spec|features)/})
    end
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.74'

  spec.add_runtime_dependency 'bauk-core', '~> 0.0.6'
  spec.add_runtime_dependency 'deep_merge', '~> 1.2'
  spec.add_runtime_dependency 'log4r', '~> 1.1'
end
