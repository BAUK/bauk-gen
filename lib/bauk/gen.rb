# frozen_string_literal: true

require 'bauk/gen/version'
require 'bauk/utils/log'
require 'bauk/utils/base_parser'
require 'bauk/gen/configs/files'
require 'bauk/gen/init'
require 'fileutils'
require 'deep_merge'


module Bauk
  module Gen
    extend Bauk::Utils::Log
    def self.parse_generators(generators = [], config = {})
      if generators.empty?
        generators = config[:generators]&.keys || []
      end
      log.error "No generators found" if generators.empty?
      generators
    end

    def self.generate(generators = [])
      config = Bauk::Gen::Configs::Files.new.generate_config
      log.debug "Using generated config: #{config}"
      generators = parse_generators generators, config

      Dir.chdir(config[:base_dir] || '.') do
        generators.each do |generator|
          load_generator(generator, config.deep_merge!(config[:generators]&.[](generator) || {})).generate
        end
      end
    end

    def self.config(generators = [])
      config = Bauk::Gen::Configs::Files.new.generate_config
      generators = parse_generators generators, config

      Dir.chdir(config[:base_dir] || '.') do
        generators.each do |generator|
          config[:generators][generator] ||= {}
          if log.debug?
            config[:generators][generator] = load_generator(generator, config.deep_merge!(config[:generators][generator])).data
          elsif log.info?
            config[:generators][generator][:config] = load_generator(generator, config.deep_merge!(config[:generators][generator])).config
          end
        end
      end
      puts YAML.dump config
    end

    def self.init(generators = [])
      generators = parse_generators generators
      if generators.empty?
        log.error "Adding default generator (You can specify init with a custom generator using the -g/--generator flag)"
        generators = ['bauk_gen']
      end
      generators.each do |generator|
        load_generator(generator, {}).init
      end
    end

    def self.parse()
      generators = []
      Bauk::Utils::BaseParser.new(
        opts: lambda do |opts|
          opts.on('-c', '--config key[=val]', 'Override config items from the command-line') do |conf|
            # TODO
            raise "--config argument is not yet implemented"
          end
          opts.on('-g', '--generator [generator-module]', 'The generators to run') do |gen|
            generators << gen
          end
        end,
        generate: {
          aliases: %i[gen g],
          info: 'Run the generation',
          action: -> { generate generators }
        },
        config: {
          aliases: %i[conf c],
          info: 'Dump the config. Add one verbose level to dump the fully generated config',
          action: -> { config generators }
        },
        init: {
          aliases: %i[i],
          info: 'Initialise the generator here with some basic examples',
          action: -> { init generators }
        }
      ).parse
    end

    def self.load_generator(module_name, config)
      parts = module_name.to_s.split(%r{-|/|::|\s+|_})
      require "#{parts.join('/').downcase}/generator"
      Object.const_get("#{parts.map(&:capitalize).join('::')}::Generator").new config
    end
  end
end
