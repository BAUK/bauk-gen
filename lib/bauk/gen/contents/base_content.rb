# frozen_string_literal: true

require 'erb'
require 'ostruct'

module Bauk
  module Gen
    module Contents
      class BaseContent
        include Bauk::Utils::Log
        def initialize(opts)
          @attributes = opts[:attributes]
          @config = opts[:config]
          @data = opts[:data]
        end

        def content
          renderer = ERB.new(File.read(@file))
          begin
            renderer.result(OpenStruct.new(@config).instance_eval { binding })
          rescue => e
            log.error("ERROR IN FILE: #{@file}")
            throw e
          end
        end

      end
    end
  end
end
