# frozen_string_literal: true

require 'bauk/gen/contents/base_content'

module Bauk
  module Gen
    module Contents
      class StringContent < BaseContent
        def initialize(opts)
          super(opts)
          @string = File.new(opts[:string])
        end
        def content
          @string
        end
      end
    end
  end
end
