# frozen_string_literal: true

require 'erb'
require 'ostruct'
require 'json'
require 'bauk/gen/contents/base_content'

module Bauk
  module Gen
    module Contents
      class ErbContent < BaseContent
        SECTION_START_REGEX = /BAUK-GEN CUSTOM SECTION ([0-9A-Z_]+) START/
        SECTION_END_REGEX   = /BAUK-GEN CUSTOM SECTION ([0-9A-Z_]+) END/

        def initialize(opts)
          super(opts)
          @file = opts[:file]
        end

        def content
          return @content if @content
          renderer = ERB.new(File.read(@file), nil, '-')
          erb_binding = OpenStruct.new(@config)
          (((@data[:contents] ||= {})[:erb] ||= {})[:mixins] ||= []).each do |mixin|
            erb_binding.extend(mixin)
          end
          begin
            @content = renderer.result(erb_binding.instance_eval { binding })
          rescue => e
            log.error("ERROR IN FILE: #{@file}")
            throw e
          end
          @content
        end

        def merge(current_content)
          type = @attributes[:merge]
          if type == true
            type = if content.match(SECTION_START_REGEX) then :section
                   elsif @file =~ /(\.json\.\..*|\.json)$/ then :json
                   elsif @file =~ /(\.ya?ml\.\..*|\.ya?ml)$/ then :yaml
                   else :section
                   end
          end
          case type.to_sym
          when :section
            merge_sections current_content, content
          when :json
            merge_json current_content, content
          when :yaml
            merge_yaml current_content, content
          else
            raise "Invalid merge type provided: #{@attributes[:merge]} for template: #{@name}"
          end
        end

        def merge_json(current_content, template_content)
          current_json = JSON.parse(current_content)
          template_json = JSON.parse(template_content)
          if @attributes[:overwrite] == false
            JSON.pretty_generate(template_json.deep_merge!(current_json))
          else
            JSON.pretty_generate(current_json.deep_merge!(template_json))
          end
        end

        def merge_yaml(current_content, template_content)
          current_json = YAML.load(current_content)
          template_json = YAML.load(template_content)
          if @attributes[:overwrite] == false
            YAML.dump(template_json.deep_merge!(current_json))
          else
            YAML.dump(current_json.deep_merge!(template_json))
          end
        end

        def merge_sections(current_content, template_content)
          sections = {}
          section = nil
          section_no = nil
          current_content.split("\n").each do |line|
            if match = line.match(SECTION_START_REGEX)
              section_no = match.captures[0]
              raise "Section #{section_no} started inside previous section for file: #{@file}" if section
              raise "Section #{section_no} has been defined more than once: #{@file}" if sections[section_no]
              section = []
            elsif match = line.match(SECTION_END_REGEX)
              raise "Section #{match.captures[0]} ended before section started for file: #{@file}" unless section
              raise "Secionn #{match.captures[0]} end block found inside section #{section_no} for file: #{@file}" unless section_no == match.captures[0]
              sections[section_no] = section.join("\n")
              section = nil
            else
              if section
                section << line
              end
            end
          end

          new_content = []
          section_no = nil
          template_content.split("\n").each do |line|
            if match = line.match(SECTION_START_REGEX)
              raise "Section #{match.captures[0]} started inside previous section for template: #{@file}" if section_no
              section_no = match.captures[0]
              new_content << line
            elsif match = line.match(SECTION_END_REGEX)
              raise "Section #{match.captures[0]} ended before section started for template: #{@file}" unless section_no
              raise "Secionn #{match.captures[0]} end block found inside section #{section_no} for template: #{@file}" unless section_no == match.captures[0]
              if sections[section_no]
                new_content.push(sections[section_no])
              else
                log.error "Section #{section_no} not found so replacing with template contents: #{@file}"
              end
              new_content << line
              section_no = nil
            else
              unless section_no and sections[section_no]
                new_content << line
              end
            end
          end
          new_content.join("\n")
        end
      end
    end
  end
end
