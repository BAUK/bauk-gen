# frozen_string_literal: true

require 'bauk/gen/contents/base_content'

module Bauk
  module Gen
    module Contents
      class FileContent < BaseContent
        attr_reader :file

        def initialize(opts)
          super(opts)
          @file = File.new(opts[:file])
        end

        def content
          @file.read
        end
      end
    end
  end
end
