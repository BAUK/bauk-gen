# frozen_string_literal: true

require 'bauk/utils/log'
require 'bauk/gen/configs/files'
require 'fileutils'

module Bauk
  module Gen
    # Class purely used for init
    module Init
      include Bauk::Utils::Log
      def init
        FileUtils.mkdir_p '.bauk/generator'
        init_templates.each do |file,content|
          file = ".bauk/generator/templates/#{name}/#{file}"
          FileUtils.mkdir_p File.dirname(file.to_s)
          File.write(file.to_s, content)
        end
        init_files.each do |file,content|
          FileUtils.mkdir_p File.dirname(file.to_s)
          File.write(file.to_s, content)
        end
        init_config_file
      end

      def init_files
        {}
      end

      def init_templates
        {
          'exampleGenerated/example..erb': <<~FILE,
            <%= name %>
          FILE
          'example_generated..erb': <<~FILE
            Name: <%= name %>
            Description: <%= description %>
          FILE
        }
      end

      # This method can be overridden to provide default values to config.
      # These should be enough to get the generator/module working and are placed into the init config file
      def base_config
        {
          config: {
            name: "ExampleName",
            description: "Example project description"
          }
        }
      end

      # Config used to init but nowhere else.
      # Useful for showing off what you can do without having defaults
      def init_config
        {
          config: {}
        }
      end

      def init_config_file
        file = Bauk::Gen::Configs::Files.new.default_files[0]
        if File.exist? file
          log.error "File already exists: #{file}"
        else
          conf = base_config
          conf[:generators] ||= {}
          conf[:generators][name.to_sym] ||= {}
          conf[:generators][name.to_sym] = default_config.deep_merge!(init_config).deep_merge!(conf[:generators][name.to_sym])
          File.write(file, YAML.dump(conf))
        end
      end
    end
  end
end
