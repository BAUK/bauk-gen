# frozen_string_literal: true

module Bauk
  module Gen
    module ConfigUtils
      # This error is to be raised every time there are errors with the config.
      # These can usually be fixed by the user in a config file
      class ConfigError < StandardError; end

      # Method that can be used to validate individual config items.
      # Takes an array of keys (or singular key) that specify the item to be checked
      #   (e.g. :a would check that {a: 123}, while [:a, :b] would check the following nested hash: {a: {b: 123}}
      # The second argument is a hash of optional things to check.
      # If this is not given, the check is merely to ensure that the value is set and not null.
      # Optional items to check are:
      #  - allow_null      : allows the value to be null or not exist
      #  - matches         : specify a regex that the item needs to match
      #  - options         : specify a list of valid options
      #  - message         : specify an optional default error message to display
      #  - null_message    : specify an optional error message to display if the value is nil
      #  - matches_message : specify an optional error message to display if the value is not in the valid list of options
      #  - options_message : specify an optional error message to display if the value does not match the given regex
      #  - default_value   : Specify a default value in case of null
      def validate_config_item(keys, map = {})
        config_item = config
        keys = [keys] unless keys.is_a? Array
        parent_keys = keys.clone
        key = parent_keys.pop
        # Get value we are referring to
        parent_keys.each do |parent_key|
          config_item[parent_key] = {} unless config_item[parent_key].is_a?(Hash)
          config_item = config_item[parent_key]
        end
        # Run checks
        if config_item[key].nil?
          if map[:default_value]
            log.debug "Setting #{keys.join('->')} to default value: #{map[:default_value]}"
            config_item[key] = map[:default_value]
          else
            map[:nil_message] ||= map[:message] || "Config item #{keys.join('->')} does not exist"
            raise ConfigError, map[:nil_message] unless map[:allow_null]
          end
        elsif map[:matches] && config_item[key] !~ (map[:matches])
          map[:matches_message] ||= map[:message] || "Config #{keys.join('->')}(#{config_item[key]}) does not match: '#{map[:matches]}'"
          raise ConfigError, map[:matches_message]
        elsif map[:options] && ! map[:options].include?(config_item[key])
          map[:options_message] ||= map[:message] || "Config #{keys.join('->')}(#{config_item[key]}) needs to be one of: '#{map[:options].join(", ")}'"
          raise ConfigError, map[:options_message]
        end
      end

      # Method to check/validate the config being provided to the generator.
      # It can, for example, be used to ensure mandatory config is present
      # or that given config is in the correct format.
      def validate_config; end

      # Function copied from online thread
      def underscore(camel_cased_word)
        camel_cased_word.to_s.gsub(/::/, '/')
                        .gsub(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
                        .gsub(/([a-z\d])([A-Z])/, '\1_\2')
                        .tr('-', '_')
                        .downcase
      end

      # This method can be overridden to provide default values to config.
      # These should be enough to get the generator/module working and are placed into the init config file
      # They are also used if they are ever removed from the config file
      def default_config
        {
          config: {}
        }
      end

      # Default config specific to this generator (injected at the generator level)
      # This config is not loaded into the config file at init but can be overwritted.
      # Can contain Ruby references.
      def default_generator_config
        {
          config: {}
        }
      end

      # This function obtains the config for this generator
      # Example config:
      # c = {
      #  name: "Project Name",
      #  description: "Project Description",
      #  name => {
      #    custom_conf: 123
      #  },
      # }
      def config
        data[:config]
      end

      def data
        if @data
          return @data if name.empty?

          return @data.deep_merge!(@data[:generators][name])
        end
        @data = base_config
            .deep_merge!(default_config)
            .deep_merge!({generators:{name => default_generator_config }})
            .deep_merge!(@input_data)
        config_generators.each do |c_gen|
          if c_gen.kind_of? Hash
            c_gen[:class].new(c_gen).add_config! @data[:config]
          else
            c_gen.new.add_config! @data[:config]
          end
        end
        unless @data[:config]
          log.error 'No config found'
        end
        log.debug "Obtained data for it's config: #{@data}"
        @data
      end
    end
  end
end
