# frozen_string_literal: true

module Bauk
  module Gen
    module Configs
      class Error < StandardError
      end
    end
  end
end
