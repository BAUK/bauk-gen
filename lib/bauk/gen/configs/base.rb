# frozen_string_literal: true

require 'bauk/utils/log'
require 'deep_merge'

module Bauk
  module Gen
    module Configs
      # Base class for all config generators.
      # Each child class needs to overwrite the following method:
      # - generate_config()
      #   -> This returns a hash of the config it obtains
      class Base
        include Bauk::Utils::Log

        # Takes optional options:
        # - keys: an array of where this config will be located in the final config hash
        def initialize(map = {})
          @keys = map[:keys] || []
        end

        # This method takes a config and adds the config that the config_generator
        #  will generate. It adds the generated config in a position on the hash
        #  dependant on which keys were passed to the config generator
        def add_config!(config, keys = @keys)
          log.debug 'adding config'
          return config.deep_merge! generate_config if keys.empty?

          key = keys.shift
          config[key] = {} if (config[key] == true) || !(config[key])
          # config[key] = add_config(config[key], keys)
          add_config!(config[key], keys)
          config
        end

        def symbolize(obj)
          if obj.is_a? Hash
            return obj.reduce({}) do |memo, (k, v)|
              memo.tap { |m| m[k.to_sym] = symbolize(v) }
            end
          elsif obj.is_a? Array
            return obj.each_with_object([]) do |v, memo|
              memo << symbolize(v)
            end
          end

          obj
        end
      end
    end
  end
end
