# frozen_string_literal: true

require 'bauk/utils/log'
require 'bauk/gen/configs/base'
require 'deep_merge'
require 'yaml'
require 'find'

module Bauk
  module Gen
    module Configs
      class Files < Base
        include Bauk::Utils::Log
        def initialize(map = {})
          super map
          @files = map[:files] || default_files
          @files << map[:extra_files] if map[:extra_files]
          log.debug "Searching for the following files: #{@files.join ', '}"
          sanitise_files
          log.debug "Files used for config: #{YAML.dump @files}"
        end

        def default_files
          [
            '.bauk/generator/config.yaml',
            '~/.bauk/generator/config.yaml'
          ]
        end

        def sanitise_files
          files_map = {}
          @files = @files.select do |file|
            path = file.is_a?(Hash) ? file[:path] : file
            File.exist?(path) or Dir.exist?(path)
          end.map do |file|
            if file.is_a?(Hash)
              file[:path] = File.expand_path file[:path]
              file
            else
              {
                path: File.expand_path(file)
              }
            end
          end
          @files.each do |file|
            log.warn("Config file included twice: '#{file}'") if files_map[file[:path]]
            files_map[file[:path]] = {}
          end
        end

        # TODO: get working with custom base key
        # Method that collects config from config files
        def generate_config
          conf = {}
          log.warn 'No config files found' if @files.empty?
          @files.each do |file_data|
            file = file_data[:path]
            if Dir.exist?(file)
              log.debug "Adding config from dir: #{file}"
              Find.find(file) do |path|
                next if Dir.exist?(path)
                next unless path =~ /\.(yaml|yml)$/
                base_keys = path.sub(/\.[a-z]*$/, '').sub(%r{^#{file}/*}, '').gsub('/', '.').split('.').map(&:to_sym)
                log.debug("Adding nested config file '#{path}' as #{base_keys.join("->")}")
                nested_config = conf
                base_keys.each do |key|
                  nested_config[key] ||= {}
                  nested_config = nested_config[key]
                end
                nested_config.deep_merge!(symbolize(YAML.load_file(path)))
              end
            else
              log.debug "Adding config from file: #{file}"
              conf.deep_merge!(symbolize((YAML.load_file(file))))
            end
          end
          conf
        end
      end
    end
  end
end
