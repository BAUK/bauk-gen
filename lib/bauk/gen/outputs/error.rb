# frozen_string_literal: true

module Bauk
  module Gen
    module Outputs
      class Error < StandardError
      end
    end
  end
end
