# frozen_string_literal: true

require 'bauk/utils/log'
require 'fileutils'
require 'bauk/gen/contents/file_content'
require 'bauk/gen/outputs/base'

module Bauk
  module Gen
    module Outputs
      class Filesystem < Base
        include Bauk::Utils::Log
        FILESYSTEM_KEYS = %s(output_base)
        Contents = Bauk::Gen::Contents

        def initialize(generator, data = {})
          super generator, data
          data[:outputs][:filesystem] ||= {}
          filesystem_config = data[:outputs][:filesystem]
          @output_base = File.expand_path(filesystem_config[:output_base] || '.')
          invalid_keys = filesystem_config.keys.reject { |key| FILESYSTEM_KEYS.include? key }
          unless invalid_keys.empty?
            raise "Invalid keys passed to Bauk::Gen::Outputs::Filesystem: #{invalid_keys.join ', '}"
          end
        end

        def default_template_dirs
          [
            '.bauk/generator/templates',
            '~/.bauk/generator/templates',
            File.expand_path('../../../../templates', __dir__)
          ]
        end

        # This function returns the absoluts path of only the provided dirs that exist
        def sanitise_dirs(dirs)
          dirs.map do |dir|
            File.expand_path("#{dir}/#{@template_name}")
          end.select do |dir|
            File.directory? dir
          end
        end

        def output_items(items)
          Dir.chdir(@output_base) do
            log.info "Outputting items to dir: #{@output_base}"
            items.each do |name, item|
              log.debug "Outputting item: #{name}(#{item})"
              if name =~ %r{/}
                parent_dir = name.sub(%r{/[^/]*$}, '')
                FileUtils.mkdir_p parent_dir
              end
              if item[:attributes][:merge] and File.exist?(name)
                merge_item name, item
              elsif item[:attributes][:overwrite] == false and File.exist?(name)
                log.info "Not overwriting file: '#{name}'"
              else
                output_item name, item
              end
            end
          end
        end

        def merge_item(name, item)
          if item[:content].respond_to? :merge
            File.write(name, item[:content].merge(File.read(name)))
          else
            log.warn "Content type for '#{name}' does not respond to merge request: #{item[:content]} (Outputting without merge)"
            output_item name, item
          end
        end

        def output_item(name, item)
          if item[:content].is_a? Contents::FileContent
            FileUtils.cp item[:content].file.path, name
          elsif item[:content].respond_to? :content
            File.write(name, item[:content].content)
          else
            raise "Invalid content found for #{name}: #{item[:content]}"
          end
        end
      end
    end
  end
end
