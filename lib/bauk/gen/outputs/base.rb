# frozen_string_literal: true

require 'bauk/utils/log'
require 'fileutils'

module Bauk
  module Gen
    module Outputs
      # Base class for all outputters
      # Each outputter needs to implement the following methods:
      # - output_items(items)
      class Base
        include Bauk::Utils::Log

        def initialize(_generator, data = {})
          data[:outputs] ||= {}
        end
      end
    end
  end
end
