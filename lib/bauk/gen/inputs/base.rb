# frozen_string_literal: true

require 'bauk/utils/log'

module Bauk
  module Gen
    module Inputs
      # Base class for inputs
      # Each input needs to overwrite the following methods:
      # - input_items(items)
      #   -> Takes a hash of items and adds
      class Base
        def input_items(_items)
          raise NotImplementedError
        end
      end
    end
  end
end
