# frozen_string_literal: true

module Bauk
  module Gen
    module Inputs
      class Error < StandardError
      end
    end
  end
end
