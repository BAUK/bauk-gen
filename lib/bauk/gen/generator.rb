# frozen_string_literal: true

require 'bauk/utils/log'
require 'bauk/gen/inputs/templates'
require 'bauk/gen/outputs/filesystem'
require 'bauk/gen/inputs/error'
require 'bauk/gen/configs/files'
require 'bauk/gen/config_utils'
require 'bauk/gen/init'
require 'deep_merge'

module Bauk
  module Gen
    # This class is the base generator that all others should extend from.
    # It contains methods for listing inputs, outputs and trnasformations.
    # The input generators cteate a map of items. These items are then handed to the output generators.
    # In the base example, the only inout is from Templates and the output is the Filesystem output. So the templates are parsed to a hash, which is then passed to any transformers. After transformation, the items are passed to the Filesystem output.
    class Generator
      include Bauk::Utils::Log
      include ConfigUtils
      include Init
      CONTENT_KEYS = %w[string file].freeze
      # Map of items
      # Example:
      # {
      #   "path/to/file" => {
      #     content: {
      #       string: "...", # Could also be file: 'path/to/source',
      #     },
      #     name: "path/to/file",
      #     attributes: {
      #       merge: true,
      #       onetime: true
      #     },
      #   }
      # }
      @items = {}

      def initialize(data)
        @input_data = data
      end

      # This method lists modules that you want to include
      def modules
        []
      end

      # This function contains the default list of configs
      def config_generators
        [
          Bauk::Gen::Configs::Files
        ]
      end

      # This function contains the default list of inputs
      def input_generators
        [
          Bauk::Gen::Inputs::Templates
        ]
      end

      # This function contains the default list of outputs
      def output_generators
        [
          Bauk::Gen::Outputs::Filesystem
        ]
      end

      # This function contains the default transformations applied to each template
      # TODO: It is still not implemented
      def transformations
        [
        ]
      end

      def generate
        log.warn "Generating #{name} generator"
        validate_config
        input_items
        validate_items
        output_items
        log.warn "Finished generating #{@items.size} items"
      end

      # This function gets the items from the inputs
      def input_items
        log.info "Generating items for generator: #{name} (#{self.class.name})"
        @items = {}
        modules.each do |mod|
          mod.input_items(@items)
        end
        input_generators.each do |i_gen|
          i_gen.new(self, data).input_items(@items)
        end
      end

      def validate_items
        @items.each do |name, item|
          item[:name] ||= name
          item[:attributes] ||= {}
          raise Inputs::Error, "No content found for item: #{name}" unless item[:content]

          unless item[:content].respond_to? :content
            raise Inputs::Error, "Invalid content found found. Found #{item[:content]}"
          end
        end
        raise Inputs::Error, 'No items found to generate' if @items.empty?
      end

      # This function writes the items to outputs
      def output_items
        output_generators.each do |o_gen|
          o_gen.new(self, data).output_items(@items)
        end
      end

      # Method to get generator name from class
      def name
        underscore(self.class.name.split('::').join('_').sub(/_*generator$/i, '')).to_sym
      end
    end
  end
end
