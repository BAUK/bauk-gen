# frozen_string_literal: true

require 'bundler/gem_tasks'
require 'rspec/core/rake_task'

RSpec::Core::RakeTask.new(:spec)

task default: :spec

task :version, [:type] do |_t, args|
  # Params
  version_file = 'lib/bauk/gen/version.rb'
  type = (args.type || :patch).to_sym
  # Get version
  version_file_contents = File.read(version_file)
  version = version_file_contents.scan(/VERSION\s*=\s*["'](.*)["']/).flatten[0]
  (major, minor, patch) = version.split('.').map(&:to_i)

  if    type == :major then major += 1; minor = 0; patch = 0
  elsif type == :minor then minor += 1; patch = 0
  elsif type == :patch then patch += 1
  else  raise 'Invalid type given. Valid types are: major, minor, patch'
  end

  puts "Incrementing #{type}: #{version} -> #{[major, minor, patch].join('.')}"
  version = [major, minor, patch].join('.')
  version_file_contents.sub!(/VERSION\s*=\s*["'].*["']/, "VERSION = '#{version}'")
  File.write(version_file, version_file_contents)
end
